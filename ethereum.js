const PRELOAD_BLOCKS = 1;

var web3 = new Web3();

web3 = new Web3(Web3.givenProvider || new Web3.providers.HttpProvider("https://api.myetherapi.com/eth"));

Notification.requestPermission(function (permission) {
	// If the user accepts, let's create a notification
	if (permission === "granted") {
		var notification = new Notification("This is example notification from real-time ETH parser");
	}
});

// Load google charts
/*google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);*/

var lastBlockHash = '';
var queue = [];
var new_queue = [];
var first_run = true;
var filter_last_saved_blockno = 0;
var test = 0;
var last_saved_block_length = 0;
var last_saved_block = '';
var search_eth_value = 30;
var highlight_big_eth_value = 500;
var biggest_transaction_value = 0;
var distinct_erc_tokens = []
var chk_eth_network = 0;

/*For TX*/
console.log('typeof EthJS:',               (typeof EthJS))
console.log('Object.keys(EthJS):',         Object.keys(EthJS))
console.log('typeof EthJS.Tx:',            (typeof EthJS.Tx))
console.log('typeof EthJS.RLP:',           (typeof EthJS.RLP))
console.log('typeof EthJS.Util:',          (typeof EthJS.Util))
console.log('typeof EthJS.Buffer:',        (typeof EthJS.Buffer))
console.log('typeof EthJS.Buffer.Buffer:', (typeof EthJS.Buffer.Buffer))

function processBlock(blockId) {
	web3.eth.getBlock(blockId, true, function(err, block) {
		if (err) {
			console.log(err);
		} else {
			if (block.hash === lastBlockHash) {
				return;
			}
			lastBlockHash = block.hash;
			var filter_current_blockno = block.number;
			if (first_run || filter_last_saved_blockno < filter_current_blockno) {
				first_run = false;
				filter_last_saved_blockno = filter_current_blockno;
				block.transactions.forEach(function(transaction) {
					queue.push({
						from: transaction.from,
						to: transaction.to,
						txHash: transaction.hash,
						value: new BigNumber(transaction.value).dividedBy(1e18).toNumber(),
						input_data: transaction.input,
						blockNumber: block.number
					});

				});

			} else {
				console.log("Same blockNo")
			}


		}
	});
}


function count_pending_tx(){
	$('#pending_tx_demo').html(web3.eth.getBlockTransactionCount("pending"))
	setTimeout(count_pending_tx, 2500)

}

function display_gas_price(){
	web3.eth.getGasPrice(function(e, r) {
		$('#gas_price_demo').html(web3.fromWei(r.c, 'gwei'));
	})
	setTimeout(display_gas_price, 4000)
}

function display_gas_limit(){
	$('#gas_limit_demo').html(web3.eth.getBlock('latest').gasLimit);
	setTimeout(display_gas_limit, 15000)
}

function display_block_no(){
	$('#block_no_demo').html(web3.eth.getBlock('latest').number);
	setTimeout(display_block_no, 2000)
}


function show_real_time_data(){
	setTimeout(display_block_no(), 500);
	setTimeout(display_gas_price(), 1000);
	setTimeout(display_gas_limit(), 2000);
	setTimeout(count_pending_tx(), 1500);
}

function display_eth_with_hidden_message(data) {
	var ul = document.getElementById("list");
	var li = document.createElement("li");
	li.appendChild(document.createTextNode("Tx Hash: " + data.txHash + " From: " + data.from + " To: " + data.to + " Value: " + data.value + " Hidden Message: " + web3.toAscii(data.input_data) + " Block no: " + data.blockNumber));
	ul.appendChild(li);
}

function display_eth_whales(data) {
	var ul = document.getElementById("whales_list");
	var li = document.createElement("li");
	li.appendChild(document.createTextNode("Tx Hash: " + data.txHash + " From: " + data.from + " To: " + data.to + " Value: " + data.value + " Hidden Message: " + web3.toAscii(data.input_data) + " Block no: " + data.blockNumber));
	if (data.value > highlight_big_eth_value) {
		li.className += " big_whale";
	}
	ul.appendChild(li);
}

function change_search_value() {
	search_eth_value = document.getElementById("search_eth_value").value;
	highlight_big_eth_value = document.getElementById("highlight_big_eth_value").value;
}

function get_erc_tokens(address, block_number, element_id) {
	$.getJSON('https://api.etherscan.io/api?module=account&action=tokentx&address=' + address + '&startblock=' + block_number + '&endblock=999999999&sort=asc&apikey=W19C2UDU9TSPMDFAN495KPQ4PRYSNHGMUC', function(data) {
		var erc_transactions = "";
		distinct_erc_tokens = [];
		erc_transactions = data.result;

		for (var i = 0; i < erc_transactions.length; i++) {
			var token_name = erc_transactions[i].tokenName;
			if (!erc_transactions.includes(token_name)) {
				distinct_erc_tokens.push(erc_transactions[i].tokenName)
			}
		}
		//$('#' + element_id).empty();
		if (distinct_erc_tokens) {
			for (var i = 0; i < distinct_erc_tokens.length; i++) {
				$('#' + element_id).append(distinct_erc_tokens[i]);
				$('#' + element_id).append("<br/>");
			}
		}
	});

}

function tickBlockchain() {
	processBlock('latest');

	var options = {  
		weekday: "long", year: "numeric", month: "short",  
		day: "numeric", hour: "2-digit", minute: "2-digit"  
	};

	
	var current_block_length = queue.length;
	//console.log("current_blocknumber_for_loop: " + queue[queue.length-1].blockNumber);
	//console.log("current_block_length_for_loop: " + current_block_length);
	if(!last_saved_block || last_saved_block_length < current_block_length) {
		try {
			last_saved_block_length = last_saved_block.length;
		}
		catch(err) {
			
		}
		last_current_block = queue.slice();
		for(var i=last_saved_block_length; i < last_current_block.length; i++) {
			//var letters = /^[.-_ a-zA-Z0-9]+$/;
			var letters = /^[!-_ a-zA-Z0-9]+$/;
			find_strings = letters.test(web3.toAscii(queue[i].input_data));
			if(biggest_transaction_value < queue[i].value) {
				biggest_transaction_value = queue[i].value;
				if(queue[i].value > 15){
					get_erc_tokens(queue[i].from, queue[i].blockNumber, 'erc_tokens');
				}
				document.getElementById("biggest_transaction_value").innerHTML = "Tx Hash: " + queue[i].txHash + " From: " + queue[i].from + " To: " + queue[i].to + " Value: " + queue[i].value + " Hidden Message: " + web3.toAscii(queue[i].input_data) + " Block no: " + queue[i].blockNumber + " Time & Date: " + new Date().toLocaleTimeString("en-us", options);
			}
			if(queue[i].value >  search_eth_value) {
				get_erc_tokens(queue[i].from, queue[i].blockNumber, 'erc_tokens_for_address_bigger_than');
				new Notification('Whale Spotted', {body: "Value: " + queue[i].value + " ETH\nTo: " + queue[i].to +  " Block no: " + queue[i].blockNumber, icon: 'https://cdn2.iconfinder.com/data/icons/cryptocurrency-5/100/cryptocurrency_blockchain_crypto-02-512.png'});
				display_eth_whales(queue[i]);
			}
			if(web3.toAscii(queue[i].input_data) && find_strings) {
				display_eth_with_hidden_message(queue[i]);
			}
		}
		last_saved_block = queue.slice();
	}

	setTimeout(tickBlockchain,3000);		

}




function convert_values(){
	var send_from = document.getElementById("send_from").value.trim().toUpperCase();
	var send_to = document.getElementById("send_to").value.trim().toUpperCase();
	var amount = document.getElementById("amount").value;
	var hidden_message = document.getElementById("hidden_message").value;
	var gas_limit = document.getElementById("gas_limit").value;
	var gas_price = document.getElementById("gas_price").value;
	var private_key = document.getElementById("private_key").value.trim().toUpperCase();
		
	document.getElementById("hex_send_from").value = web3.toHex(send_from);
	document.getElementById("hex_send_to").value = web3.toHex(send_to);
	document.getElementById("hex_amount").value = web3.toHex(web3.toWei(amount, 'ether'));
	document.getElementById("hex_hidden_message").value = web3.toHex(hidden_message);
	document.getElementById("hex_gas_limit").value = web3.toHex(web3.toWei(gas_limit, 'wei'));
	document.getElementById("hex_gas_price").value = web3.toHex(web3.toWei(gas_price, 'gwei'));
	document.getElementById("hex_private_key").value = web3.toHex(private_key);
}


function send_transaction(rawTx){
	
	var send_from = document.getElementById("hex_send_from").value;
	var send_to = document.getElementById("send_to").value;
	var hex_amount = document.getElementById("hex_amount").value;
	var hex_hidden_message = document.getElementById("hex_hidden_message").value;
	var hex_gas_limit = document.getElementById("hex_gas_limit").value;
	var hex_gas_price = document.getElementById("hex_gas_price").value;
	var hex_private_key = document.getElementById("hex_private_key").value;
	var number = web3.eth.getTransactionCount(document.getElementById("send_from").value);
	var no_nce = number+1;
	no_nce = web3.toHex(number);

	var send_tx_respnse = '';
	console.log("no_nce: "+no_nce);
	console.log("hex_gas_price: "+hex_gas_price);
	console.log("hex_gas_limit: "+hex_gas_limit);
	console.log("send_to: "+send_to);
	console.log("hex_amount: "+hex_amount);
	console.log("hex_hidden_message: "+hex_hidden_message);
	let privateKey = new EthJS.Buffer.Buffer(document.getElementById("private_key").value, 'hex')
	let txParams = {
		nonce:    no_nce,
		gasPrice: hex_gas_price, 
		gasLimit: hex_gas_limit,
		to:       send_to, 
		value:    hex_amount, 
		data:     hex_hidden_message,
		chainId: 1
	}
	let tx = new EthJS.Tx(txParams)
	tx.sign(privateKey)
	/*let serializedTx = tx.serialize().toString('hex')*/
	const serializedTx = `0x${tx.serialize().toString('hex')}`;
	/*console.log(txParams);*/

	if(!rawTx){
		$.post('https://api.etherscan.io/api', {module: 'proxy', action: 'eth_sendRawTransaction', hex: serializedTx,apikey: 'W19C2UDU9TSPMDFAN495KPQ4PRYSNHGMUC'}, function(data,error) {
			if(data.result){
				send_tx_respnse = data.result
				new Notification("Transaction Sent ! TX: " + send_tx_respnse);
			} else {
				send_tx_respnse = data.error
				new Notification("Error ! " + send_tx_respnse);
			}
			$('#tx_id').html('<b>Tx ID: </b><br/>' + send_tx_respnse)
		});
	} else {
		new Notification("RawTx: " + serializedTx);
		$('#rawTX').html('<b>RawTX: </b><br/>' + serializedTx)
	}


}

function get_address_balance(address){
	var result = web3.fromWei(web3.eth.getBalance(address));
	return result;
}

/*Find how many of duplicates are in array*/
function compressArray(original) {
 
	var compressed = [];
	// make a copy of the input array
	var copy = original.slice(0);
 
	// first loop goes over every element
	for (var i = 0; i < original.length; i++) {
 
		var myCount = 0;	
		// loop over every element in the copy and see if it's the same
		for (var w = 0; w < copy.length; w++) {
			if (original[i] == copy[w]) {
				// increase amount of times duplicate is found
				myCount++;
				// sets item to undefined
				delete copy[w];
			}
		}
 
		if (myCount > 0) {
			var a = new Object();
			a.value = original[i];
			a.count = myCount;
			compressed.push(a);
		}
	}
	return compressed;
};



function inspect_address(address){
	var tx_out = [];
	var tx_in = [];
	address = address.trim().toLowerCase();
	$('#out_tx_address').html('');
	$('#out_tx_address_count').html('');
	$('#in_tx_address').html('');
	$('#in_tx_address_count').html('');
	$('#inspected_address_balance').html('');
	var balance = get_address_balance(address);
	$('#inspected_address_balance').text(balance);
	$.getJSON('https://api.etherscan.io/api?module=account&action=txlist&address=' + address + '&startblock=0&endblock=999999999&sort=desc&apikey=W19C2UDU9TSPMDFAN495KPQ4PRYSNHGMUC', function(data) {
		var address_transactions = "";
		address_transactions = data.result;
		$('#transactions_in').html('');
		$('#transactions_out').html('');
		
		for (var i = 0; i < address_transactions.length; i++) {
			var transactions = address_transactions[i];
			if(transactions.from == $('#inspect_address').val().trim().toLowerCase()){
				$('#transactions_out').append("<br/>" + "To: " + transactions.to + " Value: " + web3.fromWei(transactions.value, 'ether'))
				tx_out.push(transactions.to);
			} else {
				$('#transactions_in').append("<br/>" + "From: " + transactions.from + " Value: " + web3.fromWei(transactions.value, 'ether'))
				tx_in.push(transactions.from);
			}
		}
		var count_address_out = compressArray(tx_out);
		for (var i = 0; i < count_address_out.length; i++) {
			$('#out_tx_address').append("<br/>" + count_address_out[i].value)
			$('#out_tx_address_count').append("<br/>" + count_address_out[i].count)
		}

		var count_address_in = compressArray(tx_in);
		for (var i = 0; i < count_address_in.length; i++) {
			$('#in_tx_address').append("<br/>" + count_address_in[i].value)
			$('#in_tx_address_count').append("<br/>" + count_address_in[i].count)
		}




	});
}


/*function enable_disable(){
	if ($('#ed_eth_network').is(':checked')){
		connect_to_web(1);
	} else {
		connect_to_web(0);
	}
}

function connect_to_web(chk_eth_network){
	if(chk_eth_network){
		web3 = new Web3(Web3.givenProvider || new Web3.providers.HttpProvider("https://api.myetherapi.com/eth"));
		if (web3.isConnected()) {
				var lastBlock = web3.eth.blockNumber;
				for (var i = lastBlock - PRELOAD_BLOCKS; i < lastBlock; i++) {
						processBlock(i);
				}
				setInterval(tickBlockchain, 3000);
				setInterval(display_gas_price, 10000);
				setInterval(display_gas_limit, 10000);
		} else {
				document.getElementById('node-info').style.visibility = 'visible';
		}	
	}
}*/

$('#ed_eth_network').change(function() {
	if ($('#ed_eth_network').is(':checked')) {
		if (web3.isConnected()) {
				var lastBlock = web3.eth.blockNumber;
				for (var i = lastBlock - PRELOAD_BLOCKS; i < lastBlock; i++) {
						processBlock(i);
				}
				/*setInterval(tickBlockchain, 3000);*/
				setTimeout(tickBlockchain,500);
		}
	}     
});
$('#ed_eth_stats').change(function() {
	if ($('#ed_eth_stats').is(':checked')) {
		setTimeout(show_real_time_data, 500);
	}     
});




